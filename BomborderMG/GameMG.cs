﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;

namespace BomborderMG
{
    public class GameMG : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        public static Point GameAreaSize = new(1250, 750);
        public const int sizeCells = 50;
        public const int speed = 10;
        public const int countDistructableWalls = 25;

        Texture2D humanTexture;
        Texture2D monsterTexture;
        Texture2D wallTexture;
        Texture2D floorTexture;
        Texture2D objectTexture; // texture for the object being destroyed
        Vector2 humanPosition = new(220,200);


        int frameWidth = 50;
        int frameHeight = 50;
        Point currentFrame = new Point(0, 0);
        Point spriteSize = new Point(8, 1);

        List<Cells> gameCells;

        Map map;
        human human;

        public GameMG()
        {
            _graphics = new GraphicsDeviceManager(this)
            {
                IsFullScreen = false,
                PreferredBackBufferWidth = GameAreaSize.X,
                PreferredBackBufferHeight = GameAreaSize.Y
            };

            IsMouseVisible = true;
            TargetElapsedTime = TimeSpan.FromSeconds(1.0 / 30.0);
        
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            map = new(GraphicsDevice.PresentationParameters.BackBufferWidth,
                          GraphicsDevice.PresentationParameters.BackBufferHeight);
            human = new(Vector2.Zero);
            gameCells = new();
            map.GenerateMap(gameCells);
            map.GenerateDistructableWalls(gameCells, countDistructableWalls);

            base.Initialize();


        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            //TODO: add another texture here
            wallTexture = Content.Load<Texture2D>("wall_texture");
            humanTexture = Content.Load<Texture2D>("human_animated_texture");
            floorTexture = Content.Load<Texture2D>("floor_texture");
            objectTexture = Content.Load<Texture2D>("distr_texture");
        }

        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState();
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                if(utilities.IsEnableMovefromDirection(utilities.Direction.Left, gameCells, new Vector2(humanPosition.X - speed, humanPosition.Y)))
                    humanPosition.X -= speed;

            }
            if (keyboardState.IsKeyDown(Keys.Right))
                if (utilities.IsEnableMovefromDirection(utilities.Direction.Right, gameCells, new Vector2(humanPosition.X + speed, humanPosition.Y)))
                    humanPosition.X += speed;

            if (keyboardState.IsKeyDown(Keys.Up))
                if (utilities.IsEnableMovefromDirection(utilities.Direction.Top, gameCells, new Vector2(humanPosition.X, humanPosition.Y + speed)))
                    humanPosition.Y -= speed;

            if (keyboardState.IsKeyDown(Keys.Down))
                if (utilities.IsEnableMovefromDirection(utilities.Direction.Bottom, gameCells, new Vector2(humanPosition.X, humanPosition.Y - speed)))
                     humanPosition.Y += speed;

            if (keyboardState.IsKeyDown(Keys.W))
                humanPosition.Y -= speed;
            if (keyboardState.IsKeyDown(Keys.A))
                humanPosition.X -= speed;
            if (keyboardState.IsKeyDown(Keys.S))
                humanPosition.Y += speed;
            if (keyboardState.IsKeyDown(Keys.D))
                humanPosition.X += speed;

            ++currentFrame.X;
            if (currentFrame.X >= spriteSize.X)
            {
                currentFrame.X = 0;
                ++currentFrame.Y;
                if (currentFrame.Y >= spriteSize.Y)
                    currentFrame.Y = 0;
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            _spriteBatch.Begin();
            foreach(var wall in gameCells)
            {
                switch (wall.cellStatus)
                {
                    case Cells.CellStatus.Wall:
                        {
                            _spriteBatch.Draw(wallTexture, wall.position,
                      new Rectangle(0, 0, (sizeCells), (sizeCells)),
                      Color.White);
                            break;
                        }
                    case Cells.CellStatus.Empty:
                        {
                            _spriteBatch.Draw(floorTexture, wall.position,
                      new Rectangle(0, 0, (sizeCells), (sizeCells)),
                      Color.White);
                            break;
                        }
                    case Cells.CellStatus.DistrWall:
                        {
                            _spriteBatch.Draw(objectTexture, wall.position,
                      new Rectangle(0, 0, (sizeCells), (sizeCells)),
                      Color.White);
                            break;
                        }
                }


            }
            _spriteBatch.Draw(humanTexture, humanPosition,
                new Rectangle(currentFrame.X * frameWidth,
                currentFrame.Y * frameHeight,
                frameWidth, frameHeight),
                Color.White, 0, Vector2.Zero,
                1, SpriteEffects.None, 0);
            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
