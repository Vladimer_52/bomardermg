﻿using System;

namespace BomborderMG
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new GameMG())
                try
                {
                    game.Run();
                }
                catch (Exception)
                {
                    Console.WriteLine("Exeption program");
                }
                
        }
    }
}
