﻿using System.Drawing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
namespace BomborderMG
{
    sealed class Cells
    {
       public CellStatus cellStatus { get; set; }
        public Vector2 position { get; set; }
        public Cells(CellStatus cellStatus, Vector2 position)
        {
            this.cellStatus = cellStatus;
            this.position = position;

        }
       public enum CellStatus
        {
            Empty = 0,
            Wall,
            DistrWall
        }
    }
}
