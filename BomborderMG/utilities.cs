﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BomborderMG
{
   static class utilities
    {
        public enum Direction
        {
            Bottom = 0,
            Left,
            Top,
            Right
        }

        private static bool IsDirectionCell(Direction dir, Vector2 humanPosition, Vector2 cellPosition) => dir switch
        {
            Direction.Left => humanPosition.X > cellPosition.X,
            Direction.Right => humanPosition.X < cellPosition.X,
            Direction.Top => humanPosition.Y < cellPosition.Y,
            Direction.Bottom => humanPosition.Y > cellPosition.Y,
            _ => throw new NullReferenceException()
        };

        public static bool IsEnableMovefromDirection(Direction dir, List<Cells> gameCells, Vector2 humanPosition)
        {
            Rectangle humanRect;
            bool isEnable = true;
            humanRect = GetTargetRect(new Vector2(humanPosition.X, humanPosition.Y));
            foreach (var cell in gameCells)
            {
                if ((cell.cellStatus == Cells.CellStatus.Wall || cell.cellStatus == Cells.CellStatus.DistrWall)
                                                         && IsDirectionCell(dir, humanPosition, cell.position))
                    if (humanRect.Intersects(GetTargetRect(cell.position)))
                    {
                        isEnable = false;
                    }
            }
            return isEnable;
        }
            private static Rectangle GetTargetRect(Vector2 target, int targetRectSize = GameMG.sizeCells)
                 => new((int)target.X - targetRectSize / 2,
                 (int)target.Y - targetRectSize / 2,
                targetRectSize,
                targetRectSize);
    }
}
