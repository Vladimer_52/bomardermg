﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace BomborderMG
{
    sealed class Map
    {
        private int _widthMap { get; set; }
        private int _heightMap { get; set; }

        Random rnd = new();

        public Map(int widthMap, int heightMap)
        {
            _widthMap = widthMap;
            _heightMap = heightMap;
        }

        public List<Cells> GenerateMap(List<Cells> mapCells)
        {

            for (int i = 0; i < _widthMap / GameMG.sizeCells; i++)
            {
                for (int j = 0; j < _heightMap / GameMG.sizeCells; j++)
                {
                    if ((i % 2 == 0) || (j % 2 == 0))
                    {
                        mapCells.Add(new Cells(Cells.CellStatus.Empty, new Vector2(i * GameMG.sizeCells, j * GameMG.sizeCells)));
                    }
                    else
                    {
                        mapCells.Add(new Cells(Cells.CellStatus.Wall, new Vector2(i * GameMG.sizeCells, j * GameMG.sizeCells)));
                    }
                }

            }
            return mapCells;
        }

        public List<Cells> GenerateDistructableWalls(List<Cells> mapCells, int countWalls)
        {
            for (int i = 0; i < countWalls; i++)
            {
                int RandomX = GameMG.sizeCells * rnd.Next(0, _widthMap / GameMG.sizeCells);
                int RandomY = GameMG.sizeCells * rnd.Next(0, _heightMap / GameMG.sizeCells);
                foreach (var cell in mapCells)
                {
                    if(cell.position.X == RandomX && cell.position.Y == RandomY && cell.cellStatus != Cells.CellStatus.Wall)
                    {
                        cell.cellStatus = Cells.CellStatus.DistrWall;
                    }
                }
            }
            return mapCells;
        }

    }
}
